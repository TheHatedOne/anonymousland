---
layout: default1
title: Changelog
permalink: /changelog
---

---

# v1.0

- Added new page ``Services.md``

- Reformatting old blog post

- New Jekyll plugin ``jekyll-feed``

- New Jekyll plugin ``jekyll-seo-tag``

- New ``_changelog`` folder along with ``changelog.md``

- Fix formatting error on ``Services.md``

- Added RSS feed for ``posts`` and for ``changelog``

- Updated file naming to be consistent

- Reformat buttons on ``index.md``
